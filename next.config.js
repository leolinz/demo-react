const withSass = require('@zeit/next-sass');

module.exports = withSass({
  sassLoaderOptions: {
    includePaths: ['styles', 'node_modules'] // This so we can use @import from node_modules in the scss files
  },
  distDir: 'build'
});
