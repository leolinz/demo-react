import { useState } from 'react';
import Header from '../component/Header';
import { withContext } from '../hocs/context';
import TableCustomers from '../component/TableCustomers';

const Dashbaord = ({ context }) => {
  const [customers, setCustomers] = useState(context.customers);
  const [accounts, setAccounts] = useState(context.accounts);
  const [results, setResults] = useState(context.accounts);

  //could move to component folder, passing setCustomer
  const searchByLastName = keyWords => {
    if (keyWords) {
      let results = customers.filter(cus => {
        return cus.lastName.toLowerCase().match(new RegExp(keyWords.toLowerCase()));
      });
      setCustomers(results);
    } else {
      setCustomers(context.customers);
    }
  };

  return (
    <section className="hero is-none is-small">
      <Header page="dashboard" />
      <div className="hero-body">
        <div className="container">
          <h1 className="title">Customers:</h1>
          <div className="columns">
            <div className="column">
              <div className="columns">
                <div className="column">
                  <div className="field has-addons">
                    <div className="control">
                      <input
                        onChange={e => searchByLastName(e.target.value)}
                        className="input"
                        type="text"
                        placeholder="Search by last name"
                      />
                    </div>
                  </div>
                </div>
              </div>
              {results.map(cus => {
                return cus.lastName;
              })}
              <table className="table">
                <thead>
                  <tr>
                    <th>Index</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Address</th>
                    <th>Phone Number</th>
                    <th>Number of account</th>
                    <th>All Account Balance</th>
                  </tr>
                </thead>

                <tbody>
                  <TableCustomers customers={customers} accounts={accounts} />
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};
export default withContext(Dashbaord);
