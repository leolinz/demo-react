//utilities
import { useState, useEffect } from 'react';
import { withRouter } from 'next/router';
import { withContext } from '../hocs/context';

//components
import Header from '../component/Header';
import TableAccounts from '../component/TableAccounts';

//funcitonal
import getCustomerByCustomerID from '../component/funtional/getCustomerByCustomerID';
import getAccountsByCustomerID from '../component/funtional/getAccountsByCustomerID';

const Customer = ({ router, context }) => {
  const [custsomer, setCustomer] = useState({});
  const [accounts, setAccounts] = useState({});
  const [loading, setLoading] = useState(true);

  useEffect(res => {
    setCustomer(getCustomerByCustomerID(router.query.customerID, context));
    setAccounts(getAccountsByCustomerID(router.query.customerID, context));
    setLoading(false);
  }, []);

  return (
    <section className="hero is-none is-small">
      <Header />
      <div className="hero-body">
        {loading ? (
          'loading...'
        ) : (
          <div className="container">
            <h1 className="title">{`${custsomer.fristName} ${custsomer.lastName}`}</h1>
            <table className="table">
              <thead>
                <tr>
                  <th>Created Date</th>
                  <th>Account ID</th>
                  <th>Customer ID</th>
                  <th>Number of Transctions</th>
                  <th>Account State</th>
                </tr>
              </thead>
              <TableAccounts accounts={accounts} updateContext={context.updateContextAccounts} />
            </table>
          </div>
        )}
      </div>
    </section>
  );
};

export default withRouter(withContext(Customer));
