import React from 'react';
import App, { Container } from 'next/app';
import Head from 'next/head';
// Global Context
import { Context } from '../hocs/context';
// Global Style
import '../style/style.scss';
import customers from '../static/customers';
import accounts from '../static/accounts';

class GoGooApp extends App {
  render() {
    const { Component, pageProps } = this.props;

    const updateContextAccounts = (proposedFiled, AccountID) => {
      // build new state object using propsedFiled
      let change = {};

      if (proposedFiled && proposedFiled.state) {
        change = {
          state: {
            close: proposedFiled.state === 'close' ? true : false,
            open: proposedFiled.state === 'open' ? true : false,
            hold: proposedFiled.state === 'hold' ? true : false
          }
        };
      }

      var foundIndex = accounts.findIndex(x => x.accountID == AccountID);
      accounts[foundIndex] = Object.assign(accounts[foundIndex], change);

      this.setState({ accounts });
    };

    this.state = { customers, accounts, updateContextAccounts };

    return (
      <Container>
        <Head>
          <title>BA - Customer protal</title>
          <meta name="robots" content="noindex" key="_index_block_all" />
          <meta name="googlebot" content="noindex" key="_index_block_google" />
        </Head>
        <Context.Provider value={this.state}>
          <div>
            <Component {...pageProps} />
          </div>
        </Context.Provider>
      </Container>
    );
  }
}

export default GoGooApp;
