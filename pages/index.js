import Header from '../component/Header';
import Link from 'next/link';

const Home = () => {
  return (
    <section className="hero is-info is-medium">
      <Header page="home" />
      <div className="hero-body">
        <div className="container has-text-left">
          <h1 className="title">Welcome to customer protal</h1>
          <h2 className="subtitle">You can view, search and edit customer</h2>
          <Link href="/dashboard">
            <a className="button is-primary">Dashboard</a>
          </Link>
        </div>
      </div>
    </section>
  );
};

export default Home;
