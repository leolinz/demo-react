import Router from 'next/router';
import numeral from 'numeral';

const TableCustomers = ({ customers, accounts }) => {
  const customerView = customerID => {
    Router.push({ pathname: '/customer', query: { customerID } });
  };

  return customers.map((cus, index) => {
    //Find out how many accounts this customer has.
    let numberOfAccounts = 0;
    let balance = 0;

    accounts.forEach(acc => {
      if (acc.customerID === cus.customerID) {
        numberOfAccounts = numberOfAccounts + 1;
      }

      if (acc.customerID === cus.customerID) {
        acc.transactions.forEach(async tranz => {
          if (tranz && tranz.amount) {
            balance = balance + tranz.amount;
          }
        });
      }
    });

    return (
      <tr onClick={() => customerView(cus.customerID)} key={index + 1}>
        <th>{index + 1}</th>
        <th>{cus.fristName}</th>
        <td>{cus.lastName}</td>
        <td>{cus.address}</td>
        <td>{cus.phone}</td>
        <td>{numberOfAccounts}</td>
        <td>$ {numeral(balance).format('0.00')}</td>
      </tr>
    );
  });
};

export default TableCustomers;
