import Link from 'next/link';
const Header = ({ page }) => {
  return (
    <div className="hero-head">
      <nav className="navbar">
        <div className="container">
          <div className="navbar-brand">
            <a className="navbar-item">
              <img src="../../static/React-logo-1.png" />
            </a>
            <span id="navbar-burger" className="navbar-burger burger" data-target="navbarMenuHeroA">
              <span />
              <span />
              <span />
            </span>
          </div>
          <div id="navbarMenuHeroA" id="navbar-menu" className="navbar-menu">
            <div className="navbar-end">
              <Link href="/">
                <a className={`navbar-item ${page === 'home' ? `is-active` : ''}`}>Home</a>
              </Link>
              <Link href="/dashboard">
                <a className={`navbar-item ${page === 'dashboard' ? `is-active` : ''}`}>Dashboard</a>
              </Link>
            </div>
          </div>
        </div>
      </nav>
    </div>
  );
};
export default Header;
