const Modal = ({ modalID, modalToggler, content }) => {
  return (
    <div id={`modal-${modalID}`} className="modal">
      <div className="modal-background" />
      <div className="modal-card">
        <header className="modal-card-head">
          <p className="modal-card-title">Transactions</p>
          <button onClick={() => modalToggler()} className="delete" aria-label="close" />
        </header>
        <section className="modal-card-body">
          <table className="table">
            <thead>
              <tr>
                <th>Index</th>
                <th>Transactions</th>
                <th>Amount</th>
              </tr>
            </thead>
            <tbody>
              {content.transactions.map((tranz, index) => {
                return (
                  <tr key={index}>
                    <th>{tranz && index + 1}</th>
                    <th>{tranz && tranz.description}</th>
                    <th>$ {tranz && tranz.amount}</th>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </section>
        <footer className="modal-card-foot">
          <button onClick={() => modalToggler()} className="button">
            Close
          </button>
        </footer>
      </div>
    </div>
  );
};

export default Modal;
