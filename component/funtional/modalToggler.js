const modalToggler = id => {
  const $modal = document.getElementById(`modal-${id}`);
  $modal.classList.toggle('is-active');
};

export default modalToggler;
