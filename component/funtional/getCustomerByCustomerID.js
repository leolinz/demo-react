const getCustomerByCustomerID = (CID, context) => {
  return context.customers.find(customer => customer.customerID == CID);
};

export default getCustomerByCustomerID;
