const getAccountsByCustomerID = (CID, context) => {
  return context.accounts.filter(acc => acc.customerID == CID);
};

export default getAccountsByCustomerID;
