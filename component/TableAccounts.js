import { useState, useEffect } from 'react';

import Modal from '../component/Modal';
import modalToggler from '../component/funtional/modalToggler';

const getState = obj => {
  return Object.keys(obj).find(key => {
    return obj[key];
  });
};

const TableAccounts = ({ accounts, updateContext }) => {
  const [localAccounts, setLocalAccounts] = useState(accounts);

  useEffect(() => {
    setLocalAccounts(accounts);
  }, [localAccounts]);

  const changeAccountStateHandler = (proposedState, accountID, setLocalAccounts) => {
    // make change to data source.
    updateContext({ state: proposedState }, accountID);

    setLocalAccounts([]);
  };

  return (
    <tbody>
      {localAccounts.map((acc, index) => {
        return (
          <tr key={index}>
            <th>{acc.createdDate}</th>
            <th>{acc.accountID}</th>
            <th>{acc.customerID}</th>
            <th>
              {acc.transactions.length} <a onClick={() => modalToggler(`transactions-${index}`)}>View Detais</a>
            </th>
            <th>
              <select
                onChange={e => changeAccountStateHandler(e.target.value, acc.accountID, setLocalAccounts)}
                value={getState(acc.state)}
              >
                <option>open</option>
                <option>close</option>
                <option>hold</option>
              </select>
            </th>
            <Modal
              modalID={`transactions-${index}`}
              modalToggler={() => modalToggler(`transactions-${index}`)}
              content={acc}
            />
          </tr>
        );
      })}
    </tbody>
  );
};

export default TableAccounts;
