import transactions from './transactions';

/**
 * Get a random integer between `min` and `max`.
 *
 * @param {number} min - min number
 * @param {number} max - max number
 * @return {number} a random integer
 */
const getRandomInt = (min, max) => {
  return Math.floor(Math.random() * (max - min + 1) + min);
};

const generateTransactions = () => {
  let randomT = [];
  let i = 0;
  let numberOfTransactions = getRandomInt(5, 30);

  do {
    // code block to be executed
    randomT.push(transactions[getRandomInt(0, 400)]);
    i++;
  } while (i <= numberOfTransactions);

  return randomT;
};
module.exports = [
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '22-05-2019',
    accountID: 1,
    state: 'open'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '18-09-2018',
    accountID: 2,
    state: 'open'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '23-05-2018',
    accountID: 3,
    state: 'hold'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '03-07-2019',
    accountID: 4,
    state: 'hold'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),

    createdDate: '08-06-2018',
    accountID: 5,
    state: 'closed'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '13-12-2019',
    accountID: 6,
    state: 'closed'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '20-04-2018',
    accountID: 7,
    state: 'open'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '03-09-2019',
    accountID: 8,
    state: 'closed'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '17-10-2019',
    accountID: 9,
    state: 'open'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '29-12-2018',
    accountID: 10,
    state: 'closed'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '05-08-2018',
    accountID: 11,
    state: 'open'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '08-11-2019',
    accountID: 12,
    state: 'closed'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '21-06-2019',
    accountID: 13,
    state: 'closed'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '04-05-2018',
    accountID: 14,
    state: 'closed'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '04-07-2019',
    accountID: 15,
    state: 'open'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '05-04-2018',
    accountID: 16,
    state: 'open'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '11-07-2018',
    accountID: 17,
    state: 'hold'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '14-02-2019',
    accountID: 18,
    state: 'closed'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '20-11-2018',
    accountID: 19,
    state: 'closed'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '19-05-2018',
    accountID: 20,
    state: 'open'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '29-05-2018',
    accountID: 21,
    state: 'open'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '16-11-2018',
    accountID: 22,
    state: 'open'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '18-05-2019',
    accountID: 23,
    state: 'open'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '31-05-2019',
    accountID: 24,
    state: 'closed'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '01-01-2019',
    accountID: 25,
    state: 'open'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '03-09-2018',
    accountID: 26,
    state: 'open'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '16-03-2018',
    accountID: 27,
    state: 'open'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '25-01-2020',
    accountID: 28,
    state: 'open'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '05-04-2019',
    accountID: 29,
    state: 'open'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '13-09-2019',
    accountID: 30,
    state: 'open'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '28-01-2019',
    accountID: 31,
    state: 'open'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '12-08-2019',
    accountID: 32,
    state: 'closed'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '14-06-2018',
    accountID: 33,
    state: 'hold'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '28-09-2019',
    accountID: 34,
    state: 'open'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '09-03-2019',
    accountID: 35,
    state: 'hold'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '04-02-2020',
    accountID: 36,
    state: 'hold'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '14-08-2019',
    accountID: 37,
    state: 'open'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '13-08-2019',
    accountID: 38,
    state: 'open'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '22-04-2019',
    accountID: 39,
    state: 'open'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '27-05-2019',
    accountID: 40,
    state: 'hold'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '18-11-2019',
    accountID: 41,
    state: 'hold'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '08-09-2018',
    accountID: 42,
    state: 'open'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '10-05-2018',
    accountID: 43,
    state: 'open'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '13-01-2020',
    accountID: 44,
    state: 'open'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '09-08-2018',
    accountID: 45,
    state: 'open'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '16-04-2018',
    accountID: 46,
    state: 'open'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '22-09-2019',
    accountID: 47,
    state: 'hold'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '02-09-2018',
    accountID: 48,
    state: 'open'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '27-07-2019',
    accountID: 49,
    state: 'open'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '04-10-2019',
    accountID: 50,
    state: 'open'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '01-09-2019',
    accountID: 51,
    state: 'closed'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '10-07-2019',
    accountID: 52,
    state: 'open'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '09-07-2019',
    accountID: 53,
    state: 'hold'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '07-03-2018',
    accountID: 54,
    state: 'open'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '24-07-2019',
    accountID: 55,
    state: 'open'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '27-01-2019',
    accountID: 56,
    state: 'open'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '30-04-2019',
    accountID: 57,
    state: 'open'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '11-09-2019',
    accountID: 58,
    state: 'closed'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '03-03-2019',
    accountID: 59,
    state: 'hold'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '21-04-2019',
    accountID: 60,
    state: 'open'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '17-03-2019',
    accountID: 61,
    state: 'closed'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '23-08-2019',
    accountID: 62,
    state: 'open'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '23-04-2019',
    accountID: 63,
    state: 'open'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '07-11-2018',
    accountID: 64,
    state: 'closed'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '27-03-2018',
    accountID: 65,
    state: 'open'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '24-06-2018',
    accountID: 66,
    state: 'open'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '26-06-2018',
    accountID: 67,
    state: 'hold'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '13-02-2020',
    accountID: 68,
    state: 'open'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '20-10-2018',
    accountID: 69,
    state: 'closed'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '12-04-2018',
    accountID: 70,
    state: 'open'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '18-10-2018',
    accountID: 71,
    state: 'open'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '18-07-2019',
    accountID: 72,
    state: 'hold'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '29-07-2019',
    accountID: 73,
    state: 'open'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '06-10-2018',
    accountID: 74,
    state: 'closed'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '09-10-2018',
    accountID: 75,
    state: 'open'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '19-08-2019',
    accountID: 76,
    state: 'hold'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '22-07-2019',
    accountID: 77,
    state: 'open'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '06-09-2019',
    accountID: 78,
    state: 'closed'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '11-03-2018',
    accountID: 79,
    state: 'hold'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '15-08-2018',
    accountID: 80,
    state: 'hold'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '15-12-2019',
    accountID: 81,
    state: 'open'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '06-06-2018',
    accountID: 82,
    state: 'open'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '05-06-2018',
    accountID: 83,
    state: 'closed'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '26-10-2019',
    accountID: 84,
    state: 'closed'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '17-01-2020',
    accountID: 85,
    state: 'hold'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '23-12-2018',
    accountID: 86,
    state: 'open'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '05-02-2020',
    accountID: 87,
    state: 'closed'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '14-04-2019',
    accountID: 88,
    state: 'open'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '21-06-2019',
    accountID: 89,
    state: 'closed'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '25-03-2018',
    accountID: 90,
    state: 'closed'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '19-05-2019',
    accountID: 91,
    state: 'hold'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '08-02-2019',
    accountID: 92,
    state: 'closed'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '28-11-2018',
    accountID: 93,
    state: 'open'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '07-11-2018',
    accountID: 94,
    state: 'open'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '24-09-2019',
    accountID: 95,
    state: 'hold'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '18-10-2019',
    accountID: 96,
    state: 'open'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '27-04-2019',
    accountID: 97,
    state: 'hold'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '26-07-2018',
    accountID: 98,
    state: 'open'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '03-02-2019',
    accountID: 99,
    state: 'hold'
  },
  {
    customerID: getRandomInt(1, 30),
    transactions: generateTransactions(),
    createdDate: '16-09-2019',
    accountID: 100,
    state: 'hold'
  }
];
