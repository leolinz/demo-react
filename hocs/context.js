import React from 'react';

export const Context = React.createContext();

export function withContext(Component) {
  return props => <Context.Consumer>{value => <Component {...props} context={value} />}</Context.Consumer>;
}
