#NextJS React Project

### Insital setup

- Project requirement:
- Assume there is back end API provide informations.
- Craete customer portal display all customer include frist name, last name, address and phone number, total over all accounts balance.
- Each customer have multiple accounts, Admin able to search customer, edit account state.

### Initial setup

1. Install nextjs and react dom
   1. Add dev build and start script in package.json
2. Create pages folder and create index.js
3. Config build folder in next.config.js
4. Install bulma
   1. Create \_app.js create style folder and style.scss file
   2. import global bulma scss in style.scss
   3. Install @zeit/next-sass
   4. Add withSass wrapper in next.config.js
5. Add babel

### Processing

Initial project see above.
Generated customer data and account data with transactions
Created context provider and consumer wrapper so Child component can access to data using withContext wrapper.
Build dashboard with total balance.
Created customer page passing parameters customerID in. Show all accounts.
Created modal show each account transactions.
i: customer page creates modals contains each account id. Toggle trigger associated modal with content passed in.

- Build customer page with account information and transactions. (completed)
- Set account status.
- Search and retrieve a customer my there name.
- Display the total balance of the customer either negative or positive (completed)

###Created functional components

1. Get customer by customer id
2. Get accounts by customer id
3. Modal Toggle passing modal id-{account-id}

###Create components

1. Header
2. Modal
3. Table of customers
4. Table of Accounts

###Search

- Using array.filter and matching regex to return target customers.

###Save account state

- passing function with context provider and call root function to setState of account state change. using useEffect to re-render.

### Run

```
npm i
npm run dev
```

### Build

```
npm run build
```
